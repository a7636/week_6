package main.executer.callable_future;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

public class Main {
    public static void main(String args[]) throws ExecutionException, InterruptedException {
        //Get ExecutorService from Executors utility class, thread pool size is 10
        ExecutorService executor = Executors.newSingleThreadExecutor();

        //Create MyCallable instance
        Callable<String> callable = new MyCallable();

        Future<String> future = executor.submit(callable);
        //add Future to the list, we can get return value using Future

        /**/
        try {
            System.out.println(new Date() + "::" + future.get(100, TimeUnit.MILLISECONDS));
        } catch (TimeoutException e) {
            System.out.println("timeout expired");
        }

        //shut down the executor service now
        executor.shutdown();
    }

}
