package main.synchronizers.sem;

import java.util.concurrent.Semaphore;

class SemaphoreDemoLock extends Thread {

    Semaphore s;
    String name;

    SemaphoreDemoLock(Semaphore s, String name){
        this.s = s;
        this.name = name;
    }

    public void run() {
        if(this.getName().equals("Thread 1")) {
            System.out.println(name + " started execution");

            try {
                System.out.println(name + " waiting to acquire permit");
                s.acquire();
                System.out.println(name + " acquired permit");

                for(int i=0;i<3;i++) {
                    Counter.count++;
                    System.out.println(name + ":" + Counter.count);
                    Thread.sleep(1000);
                }
            }
            catch(InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name + " releasing permit");
            s.release();
        }
        else {
            System.out.println(name + " started execution");

            try {
                System.out.println(name + " waiting for permit");
                s.acquire();
                System.out.println(name + " acquired permit");

                for(int i=0;i<3;i++) {
                    Counter.count--;
                    System.out.println(name + ":" + Counter.count);
                    Thread.sleep(1000);
                }
            }
            catch(InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(name + " releasing permit");
            s.release();
        }
    }
}