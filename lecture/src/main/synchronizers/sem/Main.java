package main.synchronizers.sem;

import java.util.concurrent.Semaphore;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Semaphore s = new Semaphore(1);

        SemaphoreDemoLock sd1 = new SemaphoreDemoLock(s, "Thread 1");
        SemaphoreDemoLock sd2 = new SemaphoreDemoLock(s, "Thread 2");

        sd1.start();
        sd2.start();


        sd1.join();
        sd2.join();

        System.out.println("Counter value: " + Counter.count);
    }
}
