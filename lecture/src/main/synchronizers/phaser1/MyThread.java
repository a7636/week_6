package main.synchronizers.phaser1;

import java.util.concurrent.Phaser;

class MyThread implements Runnable {
    Phaser phaser;
    String title;

    public MyThread(Phaser phaser, String title)
    {
        this.phaser = phaser;
        this.title = title;

        phaser.register();
        new Thread(this).start();
    }

    @Override public void run()
    {
        System.out.println("Thread: " + title
                + " Phase Zero Started");
        phaser.arriveAndAwaitAdvance();

        // Stop execution to prevent jumbled output
        try {
            Thread.sleep(10);
        }
        catch (InterruptedException e) {
            System.out.println(e);
        }

        System.out.println("Thread: " + title
                + " Phase One Started");
        phaser.arriveAndAwaitAdvance();

        // Stop execution to prevent jumbled output
        try {
            Thread.sleep(10);
        }
        catch (InterruptedException e) {
            System.out.println(e);
        }

        System.out.println("Thread: " + title
                + " Phase Two Started");
        phaser.arriveAndDeregister();
    }
}
