package main.synchronizers.sem1;

import java.util.concurrent.Semaphore;

class Customer implements Runnable{

    private Semaphore semaphore;
    private int customerNumber;

    public Customer(Semaphore semaphore, int customerNumber) {
        this.semaphore= semaphore;
        this.customerNumber= customerNumber;
    }
    @Override
    public void run() {
        try {
            // Trying to acquire a permit. If permit is not available , the thread will be blocked here
            semaphore.acquire();
            System.out.println("Customer:"+customerNumber +" is allowed inside the Grocery Store");
            System.out.println("Customer:"+customerNumber +" going outside the Grocery Store");

            // Release the permit.
            semaphore.release();
//            System.out.println("Customer:"+customerNumber +" goes outside the Grocery Store");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
