package main.synchronizers.sem1;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Semaphore semaphore = new Semaphore(5);
        ExecutorService executorService = Executors.newFixedThreadPool(50);
        IntStream.range(1,10).forEach(i->executorService.execute(new Customer(semaphore,i)));
        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MILLISECONDS);
    }
}
