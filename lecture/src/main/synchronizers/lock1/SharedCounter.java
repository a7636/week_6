package main.synchronizers.lock1;

import java.util.concurrent.locks.Lock;

class SharedCounter implements Runnable{
    private int c = 0;
    private Lock s;
    SharedCounter(Lock s){
        this.s = s;
    }
    // incrementing the value
    public void increment() {
        try {
            // used sleep for context switching
            Thread.sleep(10);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        c++;
    }
    // decrementing the value
    public void decrement() {
        c--;
    }

    public int getValue() {
        return c;
    }

    @Override
    public void run() {
        // acquire method to get one permit
        s.lock();
        this.increment();
        System.out.println("Value for Thread After increment - " + Thread.currentThread().getName() + " " + this.getValue());
        this.decrement();
        System.out.println("Value for Thread at last " + Thread.currentThread().getName() + " " + this.getValue());
        // releasing permit
        s.unlock();
    }
}