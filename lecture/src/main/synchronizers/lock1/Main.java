package main.synchronizers.lock1;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
    public static void main(String[] args) {
        Lock s = new ReentrantLock();
        SharedCounter counter = new SharedCounter(s);
        // Creating three threads
        Thread t1 = new Thread(counter, "Thread-A");
        Thread t2 = new Thread(counter, "Thread-B");
        Thread t3 = new Thread(counter, "Thread-C");
        t1.start();
        t2.start();
        t3.start();
    }
}
