package main.synchron.syncblock;
class MyThread extends Thread
{
    String msg;
    MyClass fobj;
    MyThread(MyClass fp, String str)
    {
        fobj = fp;
        msg = str;
        start();
    }
    public void run()
    {
        synchronized(fobj)      //Synchronized block
        {
            fobj.display(msg);
        }
    }
}