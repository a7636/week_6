package main.threads.interruption;

public class Main {
    public static void main(String[] args) {
        //create a thread instance
        StopThread stop_thread = new StopThread();
        //start the thread
        stop_thread.start();

        try {
            Thread.sleep(100);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        //interrupt the thread
        stop_thread.interrupt();
    }
}
