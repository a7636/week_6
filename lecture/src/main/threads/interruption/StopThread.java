package main.threads.interruption;

public class StopThread extends Thread{
    @Override
    public void run() {
        while (!Thread.interrupted()) {     //check for interrupted status
            System.out.println("Thread is running...");
        }
        System.out.println("Thread stopped!!!");
    }
}
