package main.threads.gr;

class ThreadDemo1_1 extends Thread{
    ThreadDemo1_1(String a, ThreadGroup b)
    {
        super(b, a);
    }
    public void run()
    {
        for (int i = 0; i< 10; i++)
        {
            try
            {
                Thread.sleep(10);
            }
            catch (InterruptedException ex)
            {
                System.out.println(Thread.currentThread().getName());
            }
        }
        System.out.println(Thread.currentThread().getName());
    }
}
