package main.threads.gr;

public class Main {
    public static void main(String[] args) {
        ThreadGroup obj1 = new ThreadGroup("Parent thread =====> ");
        ThreadGroup obj2 = new ThreadGroup(obj1, "child thread =====> ");
        ThreadDemo1_1 t1 = new ThreadDemo1_1("*******Thread-1*******", obj1);
        t1.start();
        ThreadDemo1_1 t2 = new ThreadDemo1_1("*******Thread-2*******", obj1);
        System.out.println("Total number of active thread =====> "+ obj1.activeCount());

    }
}
