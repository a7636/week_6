package main.threads.creation;

public class MyThread extends Thread{
    @Override
    public void run() {
        System.out.printf("Thread %s is running\n", Thread.currentThread().getName());
    }
}
