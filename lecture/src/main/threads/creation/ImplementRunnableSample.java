package main.threads.creation;

public class ImplementRunnableSample {


    public static void main(String[] args) {
        System.out.printf("Thread %s is running\n", Thread.currentThread().getName());

        Thread thread= new Thread(() -> System.out.printf("Thread %s is running\n", Thread.currentThread().getName()));

        thread.start();
    }
}
