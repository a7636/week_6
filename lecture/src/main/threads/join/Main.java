package main.threads.join;

public class Main {
    public static void main(String[] args)
    {
        MyThread t1=new MyThread();
        MyThread t2=new MyThread();
        t1.start();

        try{
            t1.join();	//Waiting for t1 to finish
        }catch(InterruptedException ie){}

        t2.start();

    }
}
